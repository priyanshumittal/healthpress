<?php
add_action( 'wp_enqueue_scripts', 'healthpress_theme_css',999);
function healthpress_theme_css() {
    wp_enqueue_style( 'healthpress-parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'healthpress-child-style', get_stylesheet_uri(), array( 'healthpress-parent-style' ) );
	wp_enqueue_style('flexslider', get_stylesheet_directory_uri() . '/css/flexslider/flexslider.css');
	wp_enqueue_script( 'flexslider_js', get_stylesheet_directory_uri() . '/js/flexslider/jquery.flexslider.js', array( 'jquery' ), '1.0', true );
}
	
function healthpress_remove_some_widgets(){

	// Unregister sidebars
	unregister_sidebar( 'sidebar-service' );
	unregister_sidebar( 'sidebar-project' );
}
add_action( 'widgets_init', 'healthpress_remove_some_widgets', 11 );
	
require( get_stylesheet_directory() . '/functions/widget/custom-sidebar.php' );
require( get_stylesheet_directory() . '/functions/customizer/customizer-pro.php' );
require( get_stylesheet_directory() . '/functions/customizer/slider-panel.php' );



/**
 * Enqueue script for custom customize control.
 */
function healthpress_custom_customize_enqueue() {
	wp_enqueue_script( 'healthpress_customizer_script', get_stylesheet_directory_uri() . '/js/healthcenter_customizer.js', array( 'jquery', 'customize-controls' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'healthpress_custom_customize_enqueue' );

add_action( 'customize_register', 'healthpress_remove_custom', 1000 );
function healthpress_remove_custom($wp_customize) {
  $wp_customize->remove_setting('health_pro_customizer');
  $wp_customize->remove_control('demo_Review');
  $wp_customize->remove_control('slider_post');
  
  
}

function healthpress_remove_slider_customizer() {
    remove_action( 'customize_register', 'hc_home_slider_customizer' );
}
add_action( 'after_setup_theme', 'healthpress_remove_slider_customizer', 9 );


add_action( 'after_setup_theme', 'healthpress_setup' );
   	function healthpress_setup()
   	{	
		load_theme_textdomain( 'healthpress', get_stylesheet_directory() . '/languages' );
	}

function healthpress_default_data(){
	return array(
	
	// general settings
	'home_slider_enabled'=>'on',
	'animation' => 'slide',								
	'animationSpeed' => 1500,
	'slide_direction' => 'horizontal',
	'slideshowSpeed' => 2000,
	'slider_category' => '',
	
	);
}

function healthpress_custom_excerpt() {
	
    $output = get_the_excerpt();
    $output = strip_tags(preg_replace(" (\[.*?\])",'',$output));
	$output = strip_shortcodes($output);		
	$original_len = strlen($output);
	$output = substr($output, 0, 155);		
	$len=strlen($output);
	
	if($original_len>155) {
		$output = '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>'.
					   '<div class="slide-btn-area-sm"><a href="' . get_permalink() . '" class="slide-btn-sm">'.__("Read More","healthpress").'</a></div>';
	}else{
		$output = '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>';
	}
  
  echo $output;
}
?>