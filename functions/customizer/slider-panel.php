<?php
function healthpress_home_slider_customizer( $wp_customize ) {

if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;

/**
 * A class to create a dropdown for all categories in your wordpress site
 */
 class Category_Dropdown_Custom_Control extends WP_Customize_Control
 {
    private $cats = false;

    public function __construct($manager, $id, $args = array(), $options = array())
    {
        $this->cats = get_categories($options);

        parent::__construct( $manager, $id, $args );
    }

    /**
     * Render the content of the category dropdown
     *
     * @return HTML
     */
    public function render_content()
       {
            if(!empty($this->cats))
            {
                ?>
                    <label>
                      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                      <p><select multiple <?php $this->link(); ?>>
                           <?php
                                foreach ( $this->cats as $cat )
                                {
                                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($this->value(), $cat->term_id, false), $cat->name);
									
									
                                }
                           ?>
                      </select>
					  </p>
                    </label>
                <?php
            }
       }
 }
	
	
	
/* Header Section */
	$wp_customize->add_panel( 'slider_setting', array(
		'capability'     => 'edit_theme_options',
		'priority'   => 500,
		'title'      => __('Slider Section', 'healthpress'),
	) );

	$wp_customize->add_section(
        'slider_section_settings',
        array(
            'title' => __('Setting','healthpress'),
            'description' => '',
			'panel'  => 'slider_setting',)
    );
	
			//Hide slider
			
			$wp_customize->add_setting(
			'hc_lite_options[home_slider_enabled]',
			array(
				'default' => true,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
				'type' => 'option',
			)	
			);
			$wp_customize->add_control(
			'hc_lite_options[home_slider_enabled]',
			array(
				'label' => __('Enable Home Slider','healthpress'),
				'section' => 'slider_section_settings',
				'type' => 'checkbox',
				'description' => __('Enable slider on front page.','healthpress'),
			));
			
			/* Slider Category*/
			$wp_customize->add_setting( 'hc_lite_options[slider_category]' , array(
			'type'=>'option',
			'sanitize_callback' => 'healthpress_slider_sanitize_layout',
			));
			
			$wp_customize->add_control(  new Category_Dropdown_Custom_Control( $wp_customize,'hc_lite_options[slider_category]' , array(
			'label' => __('Select posts category','healthpress'),
			'section' => 'slider_section_settings',
			'settings' => 'hc_lite_options[slider_category]',
			) ) );
	
			//Slider animation
			
			$wp_customize->add_setting(
			'hc_lite_options[animation]',
			array(
				'default' => 'slide',
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'hc_lite_options[animation]',
			array(
				'type' => 'select',
				'label' => __('Animation','healthpress'),
				'section' => 'slider_section_settings',
				'priority'   => 200,
				 'choices' => array('slide'=> 'slide', 'fade'=> 'fade'),
				));
		
		
			 //Slider animation
			
			$wp_customize->add_setting(
			'hc_lite_options[slide_direction]',
			array(
				'default' => 'horizontal',
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'hc_lite_options[slide_direction]',
			array(
				'type' => 'select',
				'label' => __('Direction','healthpress'),
				'section' => 'slider_section_settings',
				'priority'   => 250,
				'choices' => array('horizontal'=> 'horizontal', 'vertical'=>'vertical'),
				));	
		
			$wp_customize->add_setting(
			'hc_lite_options[animationSpeed]',
			array(
				'default' => '1500',
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'hc_lite_options[animationSpeed]',
			array(
				'type' => 'select',
				'label' => 'Animation speed',
				'section' => 'slider_section_settings',
				'priority'   => 300,
				 'choices' => array('500'=>'0.5','1000'=>'1.0','1500'=>'1.5','2000' => '2.0','2500' => '2.5' ,'3000' =>'3.0' , '3500' => '3.5', '4000' => '4.0','4500' => '4.5' ,'5000' => '5.0' , '5500' => '5.5' )));	
		 
			// Slide show speed
			$wp_customize->add_setting(
			'hc_lite_options[slideshowSpeed]',
			array(
				'default' => '2500',
				'type' => 'option',
				'sanitize_callback' => 'sanitize_text_field',
				
			)
			);

			$wp_customize->add_control(
			'hc_lite_options[slideshowSpeed]',
			array(
				'type' => 'select',
				'label' => __('Slideshow speed','healthpress'),
				'section' => 'slider_section_settings',
				'priority'   => 300,
				 'choices' => array('500'=>'0.5','1000'=>'1.0','1500'=>'1.5','2000' => '2.0','2500' => '2.5' ,'3000' =>'3.0' , '3500' => '3.5', '4000' => '4.0','4500' => '4.5' ,'5000' => '5.0' , '5500' => '5.5' )));	

	}
	add_action( 'customize_register', 'healthpress_home_slider_customizer' );
	
	function healthpress_slider_sanitize_layout( $value ) {
    if ( ! in_array( $value, array( 'Uncategorized','category_slider' ) ) )    
    return $value;
	}
	
	?>