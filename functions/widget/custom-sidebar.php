<?php	
add_action( 'widgets_init', 'healthpress_widgets_init');
function healthpress_widgets_init() {
	
	// Service Widget Sidebar	
	register_sidebar( array(
		'name' => __('Homepage service section - sidebar', 'healthpress' ),
		'id' => 'healthpress-sidebar-service',
		'description' => __('Use the widget WBR: HC Page Widget to add service type content','healthpress'),
		'before_widget' => '<div id="%1$s" class="col-md-4 hc_service_area widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


	//Project Sidebar
	register_sidebar( array(
			'name' => __('Homepage project section - sidebar', 'healthpress' ),
			'id' => 'healthpress-sidebar-project',
			'description' => __('Use the widget WBR: HC Page Widget to add project type content','healthpress'),
			'before_widget' => '<div id="%1$s" class="col-md-4 widget %1$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
}