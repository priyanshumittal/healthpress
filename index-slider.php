<!-- Slider -->
<?php
$current_options = wp_parse_args( get_option('hc_lite_options', array() ), healthpress_default_data());
$animation= $current_options['animation'];
$animationSpeed=$current_options['animationSpeed'];
$direction=$current_options['slide_direction'];
$slideshowSpeed=$current_options['slideshowSpeed'];
$home_slider_enabled = $current_options['home_slider_enabled'];
?>
<script>
jQuery(window).load(function() {
  // The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
	directionNav: false,
    animationLoop: false,
    slideshow: false,
    asNavFor: '#slider'
  });
   
  jQuery('#slider').flexslider({
	animation: "<?php echo $animation; ?>",
	animationSpeed: <?php echo $animationSpeed; ?>,
	direction: "<?php echo $direction; ?>",
	slideshowSpeed: <?php echo $slideshowSpeed; ?>,
    useCSS: false,
  });
});
</script>
<?php if($home_slider_enabled == true) {  
if($current_options['slider_category'] !='')
{
	
$arg = array( 'post_type'=>'post','category__in' => $current_options['slider_category'],'ignore_sticky_posts' => 1);
	$loop = new WP_Query( $arg );
	if ( $loop->have_posts() ) :
	?>
	<div class="hc_slider">
	<div id="slider" class="flexslider">
		<ul class="slides">	
		<?php 
		while ( $loop->have_posts() ) : $loop->the_post(); 
			
		if(has_post_thumbnail()):
		?>
			<li>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<?php the_post_thumbnail('home_slider', $defalt_arg); ?>	
				
				<div class="slide-caption">
					<div class="slide-text-bg1"><h1><?php the_title(); ?></h1></div>
					
					<?php if( has_excerpt() ): ?>
					<div class="slide-text-bg2">
						<?php healthpress_custom_excerpt( ); ?>
					</div>
					<?php endif; ?>
					
			</div>
				
			</li>
		<?php 
			endif;
		endwhile; 
		?>
		</ul>
	</div>
	<?php endif; echo '</div>'; }  }
	 ?>
<!-- /Slider -->