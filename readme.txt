HealthPress is the Blue color based theme on health / Dr / Clinics and small business. Give it a try you will love it

=== Description ===
In this version you will get the theme Blue color variant.

=== Support ===
For any ideas, support and feedback you can access the theme forum.

Screenshot & Banner Image : CCO by TheShiv76
- Screenshot & Banner Image URl: https://pixabay.com/en/eye-surgery-female-medical-young-766166/

Service Image : 
1. https://pixabay.com/en/medical-appointment-doctor-563427/  : CCO by DarkoStojanovic
2. https://pixabay.com/en/hospital-surgery-medical-health-721239/ : CCO by deborabalves
3. https://pixabay.com/en/research-science-laboratory-lab-1029340/ : CCO by mwooten


Based on Health-center-lite http://webriti.com/health-center-lite-version-details-page/, (C) 2012-2016 Webriti, [GPLv3 or later](https://www.gnu.org/licenses/gpl-3.0.html)

Healthpress WordPress Theme, Copyright 2016 Webriti Theme
Healthpress is distributed under the terms of the GNU GPL

The Parent theme Health-center-lite needs to be installed for this Healthpress to work.
Install
-------
1. Download "Health-center-lite" parent theme from https://wordpress.org/themes/health-center-lite/
2. Go to your WordPress dashboard and select Appearance > Install, click Upload link.
3. Upload both Health-center-lite and Healthpress Theme, then activate the Healthpress Theme


ChangeLog:
@version 1.1.3
1.Update Author URI and Theme URI change http to https secure.
@version 1.1.2
1. Updated Strings.
@version 1.1.1
1. Added Sanatization for Slider.
@version 1.1
1. Added Slider.
@version 1.0
1. Replace Column Layout 4 to 3 in service section & project section.
@version 0.6
1. Change Screenshot Image
@version 0.5
1. Updated Slider Setting.
@version 0.4
1. Solved Theme Review Issue.
@version 0.3
1. Add Blog Full Width Template.
2. Solved Theme Review Issue.
@version 0.2
1. Solved Theme Review Issue.
@version 0.1 Realesed